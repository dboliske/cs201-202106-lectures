# CS 201

This repository will contain all examples written throughout the 2021 summer semester. There may not be much at the start, but it will grow with time, like it did today.

## Course Information

This course has additional information and resources available on the [course website](http://mypages.iit.edu/~dboliske).

## Questions

If you have any questions about the code present here, please email me at [dboliske@hawk.iit.edu](mailto:dboliske@hawk.iit.edu) or message me on our course Discord.